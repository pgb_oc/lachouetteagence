//// AFTER SEO & PAGE SPEED OPTIMISATIONS ////

P4 - la chouette agence - Website SEO:

https://pgb_oc.gitlab.io/lachouetteagence/

P4 - SEO folder with audit & optimisation repport & screenshoots.

location: master branch, URI:

https://gitlab.com/pgb_oc/lachouetteagence/-/tree/master/SEO

W3c validation for CSS URI:

https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2F&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr

W3c validation for HTML URI:

https://validator.w3.org/nu/?doc=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2F

W3c validation for contact page CSS URI:

https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2Fcontact.html&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr

W3c validation for contact page HTML URI:

https://validator.w3.org/nu/?doc=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2Fcontact.html


Google PageSpeed Insights mobile URI:

https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2F

Google PageSpeed Insights desktop URI:

https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2F&tab=desktop


//// BEFORE SEO & PAGE SPEED OPTIMISATIONS ////
P4 - la chouette agence - Old website version (before optimisations) for speed test and SEO analyse: -> IMPORTANT: You need disable/comment Gzip compression for image and text in .gitlab-ci.yml (ligne 14) before use tests tools.

https://pgb_oc.gitlab.io/lachouetteagence/old-version/index.html

W3c validation for CSS URI:
https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2Fold-version%2Findex.html&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr

W3c validation for HTML URI:
https://validator.w3.org/nu/?doc=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2Fold-version%2Findex.html

Google PageSpeed Insights mobile URI:

https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2Fold-version%2Findex.html

Google PageSpeed Insights desktop URI:

https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fpgb_oc.gitlab.io%2Flachouetteagence%2Fold-version%2Findex.html&tab=desktop
